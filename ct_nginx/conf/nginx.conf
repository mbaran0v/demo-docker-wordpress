user nginx;

worker_processes auto;

pid /var/run/nginx.pid;

error_log /var/log/nginx/error.log warn;

events {
	worker_connections 1024;
	use epoll;
}

http {

	server_tokens off;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	log_format main '$remote_addr - $remote_user [$time_local] "$request" '
					'$status $body_bytes_sent "$http_referer" '
					'"$http_user_agent" "$http_x_forwarded_for"';

	log_format extended '$remote_addr - [$time_local] "$request" '
						'$status $body_bytes_sent '
						'"$http_x_forwarded_for" "$http_referer"  $host '
						'$request_time $upstream_response_time '
						'$upstream_addr - $upstream_status ';

	log_format reflog '$remote_addr - $remote_user [$time_local] '
					'"$request" $status $bytes_sent '
					'"$http_referer" "$http_user_agent" ';

	log_format timed_combined '$remote_addr - $remote_user [$time_local]  '
							'"$request" $status $body_bytes_sent '
							'"$http_x_forwarded_for" "$http_referer"  $host'
							'"$http_referer" "$http_user_agent" '
							'$request_time $upstream_response_time';

	access_log /var/log/nginx/access.log main;

	#aio threads;
	sendfile off;
	tcp_nopush on;
	tcp_nodelay off;
	ssi off;

	client_max_body_size 100m;
	client_body_buffer_size 128k;
	client_header_timeout 15;
	client_body_timeout 3m;
	client_header_buffer_size 1k;
	keepalive_timeout 30;
	large_client_header_buffers 4 16k;
	send_timeout 15;

	#server_names_hash_max_size 1024;
	#server_names_hash_bucket_size {BUCKET_SIZE};

  	gzip on;
  	gzip_http_version 1.0;
  	gzip_comp_level 5;
  	gzip_min_length 256;
  	gzip_proxied any;
  	gzip_vary on;
  	gzip_proxied expired no-cache no-store private auth;
  	gzip_disable "MSIE [1-6]\.";
  	gzip_types
		application/atom+xml
		application/javascript
		application/json
		application/rss+xml
		application/vnd.ms-fontobject
		application/x-font-ttf
		application/x-web-app-manifest+json
		application/xhtml+xml
		application/xml
		font/opentype
		image/svg+xml
		image/x-icon
		text/css
		text/plain
		text/x-component;

	ssl_ciphers ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4;
	ssl_session_cache shared:SSL:30m;
	ssl_session_timeout 5m;
	ssl_prefer_server_ciphers on;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_dhparam /etc/nginx/ssl/dhparam.pem;

	ssl_stapling on;
	ssl_stapling_verify on;
	resolver 8.8.4.4 8.8.8.8 valid=300s;
	resolver_timeout 10s;

	include conf.d/*.conf;
}