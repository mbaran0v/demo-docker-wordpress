FROM debian:jessie

MAINTAINER Maksim Y. Baranov "maxim.baranov@simbirsoft.com"

ENV DEBIAN_VERSION jessie
ENV MYSQL_MAJOR 5.6
ENV MYSQL_VERSION 5.6.30-1debian8

ENV MYSQL_REPO_URL http://repo.mysql.com/apt/debian/
ENV MYSQL_REPO_KEY_SRV ha.pool.sks-keyservers.net
ENV MYSQL_REPO_KEY A4A9406876FCBD3C456770C88C718D3B5072E1F5

RUN groupadd -r mysql && useradd -r -g mysql mysql

RUN apt-key adv --keyserver ${MYSQL_REPO_KEY_SRV} --recv-keys ${MYSQL_REPO_KEY} && \
	echo "deb ${MYSQL_REPO_URL} ${DEBIAN_VERSION} mysql-${MYSQL_MAJOR}" >> /etc/apt/sources.list

RUN { \
		echo mysql-commynity-server mysql-community-server/data-dir select ''; \
		echo mysql-commynity-server mysql-community-server/root-pass password ''; \
		echo mysql-commynity-server mysql-community-server/re-root-pass password ''; \
		echo mysql-commynity-server mysql-community-server/remove-test-db select false; \
	} | debconf-set-selections && \
	apt-get update && apt-get install -y \
		pwgen \
		mysql-server="${MYSQL_VERSION}" && \
	rm -rf /var/lib/apt/lists/* && \
	rm -rf /var/lib/mysql && \
	mkdir -p /var/lib/mysql

RUN sed -Ei 's/^(bind-address|log)/#&/' /etc/mysql/my.cnf && \
	echo 'skip-name-resolve' >> /etc/mysql/my.cnf

COPY conf/docker-entrypoint.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh && \
	ln -s /usr/local/bin/docker-entrypoint.sh /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 3306

CMD ["mysqld"]